'''
Created on 05-Nov-2016

@author: suvadeep
'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import time
import os

import tensorflow as tf




class Word_NN(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        raise NotImplementedError()
        
    
    def inference(self, initial_state, cur_batch_of_word):
        raise NotImplementedError()
        
    
    def loss(self, logits, labels):
        raise NotImplementedError()
        
        
    def count_trainable_params(self):
        total_params_count = 0
        for trainable_variable in tf.trainable_variables():
            shape = trainable_variable.get_shape()
            total_params_count += self.params_count(shape)
        return total_params_count

    def params_count(self, shape):
        '''
        Computes the total number of params for a given shape.
        '''
        params_count = 1
        for dim in shape:
            params_count = params_count*int(dim)
        return params_count
        
        
    def training(self, loss, opt_params):
        optimizer = opt_params["optimizer"]
        cn = opt_params["clip_norm"]
        
        # Add a scalar summary for the snapshot loss.
        tf.summary.scalar(loss.op.name, loss)
        # Create the gradient descent optimizer with the given learning rate.
        
        #optimizer = tf.train.AdamOptimizer(learning_rate) # lr = 1e-4
        
        # Compute the gradients for a list of variables.
        #grads_and_vars = optimizer.compute_gradients(loss)

        # grads_and_vars is a list of tuples (gradient, variable).  Do whatever you
        # need to the 'gradient' part, for example cap them, etc.
        #capped_grads_and_vars = [(tf.clip_by_norm(gv[0], cn), gv[1]) for gv in grads_and_vars]

        # Ask the optimizer to apply the capped gradients.
        #optimizer.apply_gradients(capped_grads_and_vars)
  
        # Create a variable to track the global step.
        global_step = tf.Variable(0, name='global_step', trainable=False)
        # Use the optimizer to apply the gradients that minimize the loss
        # (and also increment the global step counter) as a single training step.
        train_op = optimizer.minimize(loss, global_step=global_step)
        
        return train_op

    

    def evaluation(self, logits, labels):
        #correct = tf.nn.in_top_k(logits, labels, 1)
        # Calculate the accuracy of the prediction
        #return tf.reduce_sum(tf.cast(correct, tf.int32))
        return self.tf_confusion_metrics(logits, labels)
        
    # from https://cloud.google.com/solutions/machine-learning-with-financial-time-series-data
    def tf_confusion_metrics(self, logits, labels):
        pred = tf.argmax(logits, 1)
        actuals = labels
    
        ones_like_actuals = tf.ones_like(actuals)
        zeros_like_actuals = tf.zeros_like(actuals)
        ones_like_pred = tf.ones_like(pred)
        zeros_like_pred = tf.zeros_like(pred)

        tp = tf.reduce_sum(
            tf.cast(
                tf.logical_and(
                    tf.equal(actuals, ones_like_actuals), 
                    tf.equal(pred, ones_like_pred)
                ), 
                "float"
            )
        )
        tn = tf.reduce_sum(
            tf.cast(
                tf.logical_and(
                    tf.equal(actuals, zeros_like_actuals), 
                    tf.equal(pred, zeros_like_pred)
                ), 
                "float"
            )
        )
        fp = tf.reduce_sum(
            tf.cast(
                tf.logical_and(
                    tf.equal(actuals, zeros_like_actuals), 
                    tf.equal(pred, ones_like_pred)
                ), 
                "float"
            )
        )
        fn = tf.reduce_sum(
            tf.cast(
                tf.logical_and(
                    tf.equal(actuals, ones_like_actuals), 
                    tf.equal(pred, zeros_like_pred)
                ), 
                "float"
            )
        )
        return (tp, tn, fp, fn)

    
    def do_eval(self, 
                sess, 
                eval_correct, 
                words_placeholder, 
                labels_placeholder, 
                keep_prob_placeholder,
                data_set):
        tp_count = tn_count = fp_count = fn_count = 0
        total_count = 0
        while total_count < data_set.num_examples:
            words, labels = data_set.next_batch(self.batch_size)
            (tp, tn, fp, fn) = sess.run(eval_correct, 
                                        feed_dict={words_placeholder: words,
                                                   labels_placeholder: labels,
                                                   keep_prob_placeholder: 1})
            tp_count += tp
            tn_count += tn
            fp_count += fp
            fn_count += fn
            total_count += self.batch_size
        
        accuracy = (float(tp) + float(tn))/(float(tp) + float(fp) + float(fn) + float(tn))
        if (float(tp) + float(fn)) != 0:
            recall = float(tp)/(float(tp) + float(fn))
        else:
            recall = 1
        if (float(tp) + float(fp)) != 0:
            precision = float(tp)/(float(tp) + float(fp))
        else:
            precision = 1
        if (precision + recall) != 0:
            f1_score = (2 * (precision * recall)) / (precision + recall)
        else:
            f1_score = 0
        
        #print("accuracy = %.2f f1 score = %.2f\n" % (accuracy, f1_score))
        return (accuracy, precision, recall, f1_score)
    
    
    def run_training(self, data_sets, params):
        num_epoch = params["num_epoch"]
        res_dir = params["result_dir"]
        batch_size = params["batch_size"]
        keep_prob = params["keep_prob"]
        word_length = params["word_length"]
        num_chars = params["num_chars"]
        
        opt_params = params["opt_params"]
        
        # Save params into the res_dir
        dir = os.path.join(os.getcwd(), res_dir)
        try:
            os.stat(dir)
        except:
            os.mkdir(dir)
        with open(os.path.join(dir, 'params.txt'), 'w') as f:
            f.write(str(params))
            
        graph = tf.Graph()

        with graph.as_default():
            # Generate place holder
            words_placeholder = tf.placeholder(tf.float32, shape=(batch_size, word_length, num_chars))
            labels_placeholder = tf.placeholder(tf.int32, shape=(batch_size))
            keep_prob_placeholder = tf.placeholder(tf.float32)
    
            # Build graph
            logits = self.inference(words_placeholder, keep_prob_placeholder)
            loss = self.loss(logits, labels_placeholder)
        
            # Add to the Graph the Ops that calculate and apply gradients.
            train_op = self.training(loss, opt_params)

            # Add the Op to compare the logits to the labels during evaluation.
            eval_correct = self.evaluation(logits, labels_placeholder)

            # Build the summary operation based on the TF collection of Summaries.
            summary_op = tf.summary.merge_all()

            # Add the variable initializer Op.
            init = tf.initialize_all_variables()
        
            # Count and print total number of trainable parameters
            print("Trainable parameters: " + str(self.count_trainable_params()))
        
            # Create a saver for writing training checkpoints.
            saver = tf.train.Saver()

            # Create a session for running Ops on the Graph.
            with tf.Session(graph=graph) as sess:
                # Instantiate a SummaryWriter to output summaries and the Graph.
                # summary_writer = tf.train.SummaryWriter(res_dir, sess.graph)

                # Run the Op to initialize the variables.
                sess.run(init)

                total_duration = 0
        
                result_list = []
                for epoch in range(num_epoch):
                    start_time = time.time()
                    ep = data_sets.train.epochs_completed
                    while ep == data_sets.train.epochs_completed:
                        current_batch_of_words, current_batch_of_labels = \
                                                    data_sets.train.next_batch(batch_size)
                        feed_dict={words_placeholder: current_batch_of_words,
                                   labels_placeholder: current_batch_of_labels,
                                   keep_prob_placeholder: keep_prob}
                        _, current_loss = sess.run([train_op, loss], feed_dict=feed_dict)
            
                    duration = time.time() - start_time
                    total_duration += duration
            
                    if (epoch+1) % 50 == 0:
                        # Print status to stdout.
                        print('Epoch: %d: loss = %.2f (%.3f sec)' % (epoch+1, current_loss, duration))
       
                    # Evaluate against the training set.
                    if (epoch+1) % 50 == 0:
                        #print('Training Data Eval:')
                        (tr_a, tr_p, tr_r, tr_f) = self.do_eval(sess,
                                                       eval_correct,
                                                       words_placeholder,
                                                       labels_placeholder,
                                                       keep_prob_placeholder,
                                                       data_sets.train)

                        # Evaluate against the validation set.
                        #print('Validation Data Eval:')
                        (va_a, va_p, va_r, va_f) = self.do_eval(sess,
                                                       eval_correct,
                                                       words_placeholder,
                                                       labels_placeholder,
                                                       keep_prob_placeholder,
                                                       data_sets.validation)

                        # Evaluate against the test set.
                        #print('Test Data Eval:')
                        (te_a, te_p, te_r, te_f) = self.do_eval(sess,
                                                      eval_correct,
                                                      words_placeholder,
                                                      labels_placeholder,
                                                      keep_prob_placeholder,
                                                      data_sets.test)

                        # Save result
                        res_dict = {"train_accuracy": tr_a,
                                    "valid_accuracy": va_a,
                                    "test_accuracy": te_a,

                                    "train_f1": tr_f,
                                    "valid_f1": va_f,
                                    "test_f1": te_f,

                                    "epoch": epoch+1,
                                    "loss": current_loss,
                                    "duration": total_duration}
                        result_list.append(res_dict)

        print('Total time spent: %.3f sec' % total_duration)
        return result_list
        
        

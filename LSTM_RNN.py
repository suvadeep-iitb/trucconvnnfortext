'''
Created on 05-Nov-2016

@author: suvadeep
'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math

import tensorflow as tf
#from tensorflow.nn.rnn_cell import BasicLSTMCell, DropoutWrapper, MultiRNNCell

from Word_NN import Word_NN
import utils

 

class LSTM_RNN(Word_NN):
    '''
    classdocs
    '''


    def __init__(self, params):
        '''
        Constructor
        '''
        self.input_size = params["num_chars"]
        self.state_size = params["rec_state_size"]
        self.num_class = params["num_class"]
        self.num_layers = params["num_rec_layers"]
        self.word_length = params["word_length"]
        self.batch_size = params["batch_size"]
        self.cell = None
        
        
    def inference(self, cur_batch_of_words, keep_prob):
        def rnn_cell():
            return tf.nn.rnn_cell.BasicLSTMCell(self.state_size, state_is_tuple=True)
        def dropped_out_rnn_cell():
            return tf.nn.rnn_cell.DropoutWrapper(rnn_cell(), input_keep_prob=keep_prob, \
                                                             output_keep_prob=keep_prob)
        self.cell = tf.nn.rnn_cell.MultiRNNCell([dropped_out_rnn_cell() for _ in range(self.num_layers)], \
                                                             state_is_tuple=True)

        init_state = self.cell.zero_state(self.batch_size, tf.float32)
        seq_length = [tf.constant(self.word_length) for _ in range(self.batch_size)]
        
        # Unroll sequence statically for each word
        outputs, state = tf.nn.static_rnn(self.cell, utils.unpack_sequence(cur_batch_of_words), \
                                            dtype=tf.float32, initial_state=None, \
                                            sequence_length=None)
        output = outputs[-1]
       
        # Compute softmax
        weights_sm = tf.Variable(tf.truncated_normal([self.state_size, self.num_class], \
                                                stddev=1.0 / math.sqrt(float(self.state_size))))
        biases_sm = tf.Variable(tf.zeros([self.num_class]))
        logits = tf.matmul(output, weights_sm) + biases_sm
        
        return logits
        
    
    def loss(self, logits, labels):
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=labels, name='xentropy')
        loss = tf.reduce_mean(cross_entropy, name='xentropy_mean')
        
        return loss


'''
Created on 10-Nov-2016

@author: suvadeep
'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

from Word_NN import Word_NN
import utils

 

class TCNN_RNN(Word_NN):
    '''
    classdocs
    '''


    def __init__(self, params):
        '''
        Constructor
        '''
        # Initialize general parameters
        self.input_size = params["num_chars"]
        self.num_class = params["num_class"]
        self.word_length = params["word_length"]
        self.batch_size = params["batch_size"]

        # Initialize convolutional layer parameters
        self.conv_layer_depth = params["conv_layer_depth"]
        self.conv_width = params["conv_width"]
        self.conv_act_func = params["conv_act_func"]
        self.num_conv_layers = params["num_conv_layers"]
        
        # Initialize recurrent layer parameters
        self.rec_state_size = params["rec_state_size"]
        self.num_rec_layers = params["num_rec_layers"]
        
        self.cell = None
        
        
    def initialize_rnn_cell(self, keep_prob):
        self.cell = tf.nn.rnn_cell.BasicLSTMCell(self.rec_state_size, state_is_tuple=True)
        self.cell = tf.nn.rnn_cell.DropoutWrapper(self.cell, input_keep_prob=keep_prob, \
                                                             output_keep_prob=keep_prob)
        self.cell = tf.nn.rnn_cell.MultiRNNCell([self.cell] * self.num_rec_layers, \
                                                             state_is_tuple=True)

        
    def inference(self, cur_batch_of_words, keep_prob):
        # Construct convolutional layers
        next_in = cur_batch_of_words

        depth_in = [self.input_size]
        depth_in.extend(self.conv_layer_depth[:-1])
        depth_out = self.conv_layer_depth

        for l in range(self.num_conv_layers):
            with tf.name_scope('conv_layers-%d' % l):
                # Declare the weight variables
                W_conv = utils.weight_variable([self.conv_width[l], depth_in[l], \
                                                                    depth_out[l]])
                b_conv = utils.bias_variable([depth_out[l]])
            
                # Construct the graph of convolution layer
                conv = utils.conv1d(next_in, W_conv) + b_conv
                next_in = self.conv_act_func(conv)
                
                # next_in = tf.nn.dropout(next_in, 0.8)
        
        # Construct recurrent layers
        with tf.name_scope('rec_layers'):
            # Initialize the rnn cells
            self.initialize_rnn_cell(keep_prob)
        
            init_state = self.cell.zero_state(self.batch_size, tf.float32)
            seq_length = [tf.constant(self.word_length)]*self.batch_size
        
            # Unroll recurrent unit statically for each word
            outputs, state = tf.nn.static_rnn(self.cell, utils.unpack_sequence(next_in), \
                                              dtype=tf.float32, initial_state=init_state, \
                                              sequence_length=seq_length)
            output = outputs[-1]
        
        # Compute softmax
        with tf.name_scope('linear_layers'):
            weights_sm = utils.weight_variable([self.rec_state_size, self.num_class])
            biases_sm = utils.bias_variable([self.num_class])
            logits = tf.matmul(output, weights_sm) + biases_sm
        
        return logits


    def loss(self, logits, labels):
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=labels, name='xentropy')
        loss = tf.reduce_mean(cross_entropy, name='xentropy_mean')
        
        return loss


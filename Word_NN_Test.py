'''
Created on 05-Nov-2016

@author: suvadeep
'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import time
import os

import tensorflow as tf

from Input_Data import Data_Sets 



class Word_NN(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        raise NotImplementedError()
        
    
    def inference(self, initial_state, cur_batch_of_word):
        raise NotImplementedError()
        
    
    def loss(self, logits, labels):
        raise NotImplementedError()
    

    def evaluation(self, logits, labels):
        correct = tf.nn.in_top_k(logits, labels, 1)
        # Return the number of true entries.
        return tf.reduce_sum(tf.cast(correct, tf.int32))
    
    
    def do_eval(self, 
                sess, 
                eval_correct, 
                words_placeholder, 
                labels_placeholder, 
                keep_prob_placeholder,
                data_set):
        true_count = 0  # Counts the number of correct predictions.
        total_count = 0
        while data_set.epochs_completed() == 0:
            current_batch_of_data = data_set.next_batch()
            true_count += sess.run(eval_correct, 
                                   feed_dict={words_placeholder: current_batch_of_data.words(),
                                              labels_placeholder: current_batch_of_data.labels(),
                                              keep_prob_placeholder: 1})
            total_count += current_batch_of_data.num_examples()
        
        precision = true_count / total_count
        print('  Num examples: %d  Num correct: %d  Precision @ 1: %0.04f\n' %
                  (total_count, true_count, precision))
        return precision
    
    
    def run_training(self, data_sets, params):
        num_epoch = params["num_epoch"]
        res_dir = params["result_dir"]
        batch_size = params["batch_size"]
        keep_prob = params["keep_prob"]
        word_length = params["word_length"]
        num_chars = params["num_chars"]
        
        #data_sets = Data_Sets()
        #data_sets.read_data_sets(params)
        
        # Generate place holder
        words_placeholder = tf.placeholder(tf.float32, shape=(batch_size, word_length, num_chars))
        labels_placeholder = tf.placeholder(tf.int32, shape=(batch_size))
        keep_prob_placeholder = tf.placeholder(tf.float32)
    
        # Build graph
        op = self.inference(words_placeholder, keep_prob_placeholder)
        #loss = self.loss(logits, labels_placeholder)
        
        # Add the Op to compare the logits to the labels during evaluation.
        #eval_correct = self.evaluation(logits, labels_placeholder)

        # Build the summary operation based on the TF collection of Summaries.
        #summary_op = tf.merge_all_summaries()

        # Add the variable initializer Op.
        init = tf.initialize_all_variables()
        
        # Create a saver for writing training checkpoints.
        #saver = tf.train.Saver()

        # Create a session for running Ops on the Graph.
        sess = tf.Session()

        # Instantiate a SummaryWriter to output summaries and the Graph.
        #summary_writer = tf.train.SummaryWriter(res_dir, sess.graph)

        # Run the Op to initialize the variables.
        sess.run(init)

        for epoch in range(num_epoch):
            while epoch == data_sets.train.epochs_completed:
                current_batch_of_words, current_batch_of_labels = \
                                                            data_sets.train.next_batch(batch_size)
                in_st = sess.run(op, \
                                 feed_dict={words_placeholder: current_batch_of_words, \
                                                    keep_prob_placeholder: keep_prob})

        sess.close()
        
        return in_st
        

'''
Created on 05-Nov-2016

@author: suvadeep

Modified version of input_data.py from https://github.com/tensorflow/tensorflow/tree/master/tensorflow/examples/tutorials/mnist
'''

import numpy as np
import random
import os
import json

class Data_Set(object):
    def __init__(self, words, labels):
        self._words = words
        self._labels = labels
        self._epochs_completed = 0
        self._index_in_epoch = 0
        self._num_examples = words.shape[0]

    @property
    def words(self):
        return self._words

    @property
    def labels(self):
        return self._labels

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def next_batch(self, batch_size):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        self._index_in_epoch += batch_size
        if self._index_in_epoch > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Shuffle the data
            perm = np.arange(self._num_examples)
            np.random.shuffle(perm)
            self._words = self._words[perm]
            self._labels = self._labels[perm]
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size <= self._num_examples
        end = self._index_in_epoch
        return self._words[start:end], self._labels[start:end]


class Data_Sets(object):
    def __init__(self):
        self.train = []
        self.validation = []
        self.test = []

    def write_params(self, dir, params):
        '''
        with open(os.path.join(dir, 'params.txt'), 'w') as f:
            f.write('# NUM_CHARS \t\t\t: ' + str(params["num_chars"]) + '\n')
            f.write('# WORD_LENGTH \t\t\t: ' + str(params["word_length"]) + '\n')
            f.write('# CHAR_MAPPING \t\t\t: ' + ' '.join(params["char_mapping"]) + '\n')
        
            f.write('# DATA_DIR \t\t\t: ' + params["data_dir"] + '\n')
            f.write('# NUM_TRAINING_EXAMPLES \t: ' + str(params["num_train_examples"]) + '\n')
            f.write('# NUM_VALIDATION_EXAMPLES \t: ' + str(params["num_validation_examples"]) + '\n')
            f.write('# NUM_TEST_EXAMPLES \t\t: ' + str(params["num_test_examples"]) + '\n')
        
            f.write('# TARGET_PATTERNS \t\t: ' + ''.join(params["target_patterns"]) + '\n')
            f.write('# RANDOM_GEN_SEED \t\t: ' + str(params["random_generator_seed"]) + '\n')
            f.write(str(params))
        '''
        json.dump(params, open(os.path.join(dir, 'params.txt'), 'w'))
        

    def simulate_data_fixed_pos_pattern(self, params):
        num_chars = params["num_chars"]
        word_length = params["word_length"]
        char_mapping = params["char_mapping"]
        
        data_dir = params["data_dir"]
        num_train_examples = params["num_train_examples"]
        num_validation_examples = params["num_validation_examples"]
        num_test_examples = params["num_test_examples"]
        
        target_patterns = params["target_patterns"]
        pattern_occur_prob = params["pattern_occur_prob"]
        pattern_occur_index = params["pattern_occur_index"]
        random_gen_seed = params["random_generator_seed"]
        
        random.seed(random_gen_seed)
        total_examples = num_train_examples + num_validation_examples + num_test_examples
        words = []
        for i in range(total_examples):
            w = [char_mapping[random.randint(0, num_chars - 1)] for c in range(word_length)]
            w = ''.join(w)
            # Insert the target pattern with probability 'pattern_occur_prob'
            if random.uniform(0, 1) < pattern_occur_prob * len(target_patterns):
                pet_idx = random.randint(0, len(target_patterns)-1)
                w = w[:pattern_occur_index] + target_patterns[pet_idx] + \
                    w[pattern_occur_index + len(target_patterns[pet_idx]):]
            words.append(w)
        
        labels = []
        for w in words:
            flag = 0
            if target_patterns[0] in w:
                flag = 1
            labels.append(flag)

        
        dir = os.path.join(os.getcwd(), data_dir)
        try:
            os.stat(dir)
            user_input = raw_input('Data directory already exists! Do you want to overwrite it?\nType "y" if yes, "n" otherwise:')
            if user_input != 'y' and user_input != 'Y':
                return
        except:
            os.mkdir(dir)

        self.write_params(dir, params);                
        
        words_file = os.path.join(dir, 'words.txt')
        with open(words_file, 'w') as f:
            f.writelines([w + '\n' for w in words])
        labels_file = os.path.join(dir, 'labels.txt')
        with open(labels_file, 'w') as f:
            f.writelines([str(l) + '\n' for l in labels])


    def simulate_data_rand_pos_pattern(self, params):
        num_chars = params["num_chars"]
        word_length = params["word_length"]
        char_mapping = params["char_mapping"]
        
        data_dir = params["data_dir"]
        num_train_examples = params["num_train_examples"]
        num_validation_examples = params["num_validation_examples"]
        num_test_examples = params["num_test_examples"]
        
        target_patterns = params["target_patterns"]
        pattern_occur_prob = params["pattern_occur_prob"]
        random_gen_seed = params["random_generator_seed"]
        
        random.seed(random_gen_seed)
        total_examples = num_train_examples + num_validation_examples + num_test_examples
        words = []
        for i in range(total_examples):
            w = [char_mapping[random.randint(0, num_chars - 1)] for c in range(word_length)]
            w = ''.join(w)
            # Insert the target pattern with probability 'pattern_occur_prob'
            if random.uniform(0, 1) < pattern_occur_prob * len(target_patterns):
                pat_idx = random.randint(0, len(target_patterns)-1)
                pos_idx = random.randint(0, len(w)-len(target_patterns[pat_idx])-1)
                w = w[:pos_idx] + target_patterns[pat_idx] + \
                    w[pos_idx + len(target_patterns[pat_idx]):]
            words.append(w)
        
        labels = []
        for w in words:
            flag = 0
            if target_patterns[0] in w:
                flag = 1
            labels.append(flag)

        
        dir = os.path.join(os.getcwd(), data_dir)
        try:
            os.stat(dir)
            user_input = raw_input('Data directory already exists! Do you want to overwrite it?\nType "y" if yes, "n" otherwise:')
            if user_input != 'y' and user_input != 'Y':
                return
        except:
            os.mkdir(dir)

        self.write_params(dir, params);                
        
        words_file = os.path.join(dir, 'words.txt')
        with open(words_file, 'w') as f:
            f.writelines([w + '\n' for w in words])
        labels_file = os.path.join(dir, 'labels.txt')
        with open(labels_file, 'w') as f:
            f.writelines([str(l) + '\n' for l in labels])

        
                
    def read_text(self, words_file):
        with open(words_file) as f:
            words = [w.strip() for w in f.readlines()]
        
        char_set = sorted(set(''.join(words)))
        char_mapping = {ch: i for i, ch in enumerate(char_set)}
        
        data = np.zeros((len(words), len(words[0]), len(char_set)), np.int8)
        for i, w in enumerate(words):
            for j, ch in enumerate(w):
                data[i, j, char_mapping[ch]] = 1 
                
        return data
        
        
        
    def read_labels(self, labels_file):
        with open(labels_file) as f:
            labels = [int(l) for l in f.readlines()]
        labels = np.array(labels, np.int8)
        return labels

    
    
    def read_data_sets(self, params):
        data_dir = params["data_dir"]
        num_train_examples = params["num_train_examples"]
        num_validation_examples = params["num_validation_examples"]
        num_test_examples = params["num_test_examples"]
        word_length = params["word_length"]
        num_chars = params["num_chars"]
        
        # Check whether the parameters match or not
        data_set_params = json.loads(open(os.path.join(data_dir, 'params.txt')).read())
        assert params["num_chars"] == data_set_params["num_chars"], \
                                                    'Dataset parameter mismatch'
        assert params["word_length"] == data_set_params["word_length"], \
                                                    'Dataset parameter mismatch'
        assert params["num_class"] == data_set_params["num_class"], \
                                                    'Dataset parameter mismatch'
        assert params["num_input_symbols"] == data_set_params["num_input_symbols"], \
                                                    'Dataset parameter mismatch'

        
        words = self.read_text(os.path.join(data_dir, 'words.txt'))
        labels = self.read_labels(os.path.join(data_dir, 'labels.txt'))

        assert words.shape[2] == num_chars, 'Inconsistent datasets'
        assert words.shape[0] == labels.shape[0], 'Inconsistent datasets'
        assert word_length == words.shape[1], 'Inconsistent datasets'
        assert labels.shape[0] >= (num_train_examples+num_validation_examples+ \
                        num_test_examples), 'Not enough examples in the file'
                        
        # Perform an initial random permutation
        perm = np.arange(labels.shape[0])
        np.random.shuffle(perm)
        words = words[perm]
        labels = labels[perm]
            
        self.train = Data_Set(words[:num_train_examples], labels[:num_train_examples])
        self.validation = Data_Set(words[num_train_examples: num_train_examples+num_validation_examples], 
                                labels[num_train_examples: num_train_examples+num_validation_examples])
        self.test = Data_Set(
                    words[num_train_examples+num_validation_examples: num_train_examples+num_validation_examples + num_test_examples], \
                    labels[num_train_examples+num_validation_examples: num_train_examples+num_validation_examples + num_test_examples])
        

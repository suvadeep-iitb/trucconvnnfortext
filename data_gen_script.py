from Input_Data import Data_Sets


params = {}

# Model parameters
params["num_chars"] = 26
params["word_length"] = 20
params["num_class"] = 2
params["num_input_symbols"] = params["num_chars"]

# Dataset parameters
# ABCDE_ABC_BCD_CDE_ABCD_BCDE
params["target_patterns"] = ['ABCDE', 'ABC', 'BCD', 'CDE', 'ABCD', 'BCDE'] # Only the first one is the pattern
                                                                           # appears in the positive examples
params["pattern_occur_prob"] = 0.1
params["random_generator_seed"] = 13
params["char_mapping"] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', \
                          'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', \
                          'U', 'V', 'W', 'X', 'Y', 'Z']

params["num_train_examples"] = 200
params["num_validation_examples"] = 500
params["num_test_examples"] = 500

data_sets = Data_Sets()

isRandom = False
if isRandom == False:
    params["dataset_type"] = "FIXED_POS_PATTERN"
    params["pattern_occur_index"] = 15
    params["data_dir"] = params["dataset_type"] + '_' + str(params["pattern_occur_index"]) +\
                                                  '_' + '_'.join(params["target_patterns"])
    data_sets.simulate_data_fixed_pos_pattern(params)
else:
    params["dataset_type"] = "RAND_POS_PATTERN"
    params["data_dir"] = params["dataset_type"] + '_' + '_'.join(params["target_patterns"])
    data_sets.simulate_data_rand_pos_pattern(params)

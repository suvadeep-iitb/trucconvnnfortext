from Input_Data import Data_Sets
from LSTM_RNN import LSTM_RNN
from TCNN_RNN import TCNN_RNN
from CNN import CNN
import tensorflow as tf

import pickle

params = {}

# Model parameters
params["num_chars"] = 26
params["word_length"] = 20
params["num_class"] = 2
params["num_input_symbols"] = params["num_chars"]

params["num_train_examples"] = 200
params["num_validation_examples"] = 500
params["num_test_examples"] = 500

params["data_dir"] = "RAND_POS_PATTERN_ABCDE_ABC_BCD_CDE_ABCD_BCDE"

# Training parameters
params["num_epoch"] = 500


N_EXP = 1
# Global parameter setting

full_results = []
for exp in range(N_EXP):
    data_sets = Data_Sets()
    data_sets.read_data_sets(params)

    params["batch_size"] = 100
    optimizer = tf.train.GradientDescentOptimizer(0.1)

    keep_prob = [.5, .75, 1]

    # Global parameter setting ends

    # Train LSTM_RNN 

    num_rec_layers = [2, 3, 4]
    rec_state_size = [100, 150]

    
    params["result_dir"] = "RESULT_LSTM_RNN_" + params["data_dir"]

    total_iter = len(num_rec_layers)*len(rec_state_size)*len(keep_prob)
    lstm_results = []
    iter_count = 0
    for rl in range(len(num_rec_layers)):
        ss_list = []
        for ss in range(len(rec_state_size)):
            kp_list = []
            for kp in range(len(keep_prob)):
                params["num_rec_layers"] = num_rec_layers[rl]
                params["rec_state_size"] = rec_state_size[ss]
                params["keep_prob"] = keep_prob[kp]
                params["opt_params"] = {"optimizer": optimizer,
                                        "clip_norm": float("inf")}
                        
                rnn = LSTM_RNN(params)
                res = rnn.run_training(data_sets, params)
                p = params
                p["opt_params"] = 0.1
                kp_list.append((res, p))
                        
                iter_count += 1
                print("\n\n\nEXP LSTM %d: Completed Iterations: %d of %d\n\n\n" % 
                                                      (exp+1, iter_count, total_iter))
                                        
            ss_list.append(kp_list)
        lstm_results.append(ss_list)
    pickle.dump(lstm_results, open('lstm_results.txt', 'w+'))
    

    
    # Train TCNN_RNN

    params["num_rec_layers"] = 1
    rec_state_size = [100]

    conv_layer_depth = [[50, 50],
                        [100, 100],
                        [50, 50, 50],
        	        [100, 100, 100]]
    conv_width = 2
    params["conv_act_func"] = tf.nn.relu
    
    params["result_dir"] = "RESULT_TCNN_RNN_" + params["data_dir"]

    total_iter = len(rec_state_size)*len(conv_layer_depth)*len(keep_prob)
    tcnn_results = []
    iter_count = 0
    for rss in range(len(rec_state_size)):
        cld_list = []
        for cld in range(len(conv_layer_depth)):
            kp_list = []
            for kp in range(len(keep_prob)):
                params["rec_state_size"] = rec_state_size[rss]
                params["conv_layer_depth"] = conv_layer_depth[cld]
                params["conv_width"] = [conv_width for n in range(len(conv_layer_depth[cld]))]
                params["num_conv_layers"] = len(conv_layer_depth[cld])
                params["keep_prob"] = keep_prob[kp]
                params["opt_params"] = {"optimizer": optimizer, "clip_norm": float("inf")}
 		
                rnn = TCNN_RNN(params)
                res = rnn.run_training(data_sets, params)
                p = params
                p["opt_params"] = 0.1
                kp_list.append((res, p))
                        
                iter_count += 1
                print("\n\n\nEXP TCNN %d: Completed Iterations: %d of %d\n\n\n" % 
                                                                (exp+1, iter_count, total_iter))
                                        
            cld_list.append(kp_list)
        tcnn_results.append(cld_list)
    pickle.dump(tcnn_results, open('tcnn_results.txt', 'w+'))
    
    
    # Train CNN

    feature_sizes = [50, 100] #, 200]
    ngram_sizes = [[1, 2, 3], \
                   [1, 2, 3, 4], \
       		   [1, 2, 3, 4, 5]]
    conv_act_func      = [tf.nn.relu]
    conv_act_func_name = ['relu']

    params["result_dir"] = "RESULT_CNN_" + params["data_dir"]

    total_iter = len(ngram_sizes)*len(feature_sizes)*len(conv_act_func)*len(keep_prob)
    cnn_results = []
    iter_count = 0
    for gs in range(len(ngram_sizes)):
        fs_list = []
        for fs in range(len(feature_sizes)):
            ac_list = []
            for ac in range(len(conv_act_func)):
                kp_list = []
                for kp in range(len(keep_prob)):
                    params["ngram_sizes"] = ngram_sizes[gs]
                    params["feature_sizes"] = [feature_sizes[fs] for i in range(len(ngram_sizes[gs]))]
                    params["conv_act_func"] = conv_act_func[ac]
                    params["keep_prob"] = keep_prob[kp]
                    params["opt_params"] = {"optimizer": optimizer, "clip_norm": float("inf")}

                    cnn = CNN(params)
                    res = cnn.run_training(data_sets, params)
                    del cnn
                    p = params
                    p["opt_params"] = 0.1
                    p["conv_act_func"] = conv_act_func_name[ac]
                    kp_list.append((res, p))

                    iter_count += 1
                    print("\n\n\nEXP CNN %d: Completed Iterations: %d of %d\n\n\n" % 
                                                            (exp+1, iter_count, total_iter))
                                        
                ac_list.append(kp_list)
            fs_list.append(ac_list)
        cnn_results.append(fs_list)
    pickle.dump(cnn_results, open('cnn_results.txt', 'w+')) 
    
    full_results.append({
                         'lstm': lstm_results
                         ,
                          'tcnn': tcnn_results
                         ,
                         'cnn' : cnn_results
		        })
    pickle.dump(full_results, open('p_full_results.txt', 'w+'))
pickle.dump(full_results, open('full_results.txt', 'w+'))




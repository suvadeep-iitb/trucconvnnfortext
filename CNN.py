'''
Created on 10-Nov-2016

@author: suvadeep
'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

from Word_NN import Word_NN
import utils

 

class CNN(Word_NN):
    '''
    classdocs
    '''


    def __init__(self, params):
        '''
        Constructor
        '''
        # Initialize general parameters
        self.input_size = params["num_chars"]
        self.num_class = params["num_class"]
        self.word_length = params["word_length"]
        self.batch_size = params["batch_size"]

        # Initialize convolutional layer parameters
        self.ngram_sizes = params["ngram_sizes"]
        self.feature_sizes = params["feature_sizes"]
        self.conv_act_func = params["conv_act_func"]
        
        
    def inference(self, cur_batch_of_words, keep_prob):
        next_in = cur_batch_of_words
        pooled_outputs = []
        for i, ngram_size in enumerate(self.ngram_sizes):
            with tf.name_scope("conv_layer-%d" % ngram_size):
                # Convolution Layer
                W_conv = utils.weight_variable([ngram_size, self.input_size,\
                                                                self.feature_sizes[i]])
                b_conv = utils.bias_variable([self.feature_sizes[i]])
                h = self.conv_act_func(utils.conv1d(next_in, W_conv) + b_conv)

                # Add max-pooling dropout
                # http://oemmndcbldboiebfnladdacbdfmadadm/https://arxiv.org/pdf/1512.00242.pdf
                h_drop = tf.nn.dropout(h, keep_prob)

                # Max-pooling over the outputs
                pooled = utils.max_pool1d(h_drop, ksize=[1, self.word_length, 1])
                pooled_outputs.append(tf.expand_dims(pooled, 2))
 
        # Combine all the pooled features
        self.total_features = sum(self.feature_sizes)
        h_pool = tf.concat(axis=2, values=pooled_outputs)
        h_pool_flat = tf.reshape(h_pool, [self.batch_size, -1])
        
        # Add dropout
        with tf.name_scope("dropout"):
            h_drop = tf.nn.dropout(h_pool_flat, keep_prob)
        
        # Compute softmax
        with tf.name_scope('linear_layers'):
            weights_sm = utils.weight_variable([self.total_features, self.num_class])
            biases_sm = utils.bias_variable([self.num_class])
            logits = tf.matmul(h_drop, weights_sm) + biases_sm
        
        return logits


    def loss(self, logits, labels):
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=labels, name='xentropy')
        loss = tf.reduce_mean(cross_entropy, name='xentropy_mean')
        
        return loss

